package com.example.milestone4.model;

public class Contact implements Comparable<Contact> {
	
	protected String firstName;
	protected String lastName;
	protected String phoneNum;
	protected int age;
	protected int pictureNumber;
	protected String loc;

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getPictureNumber() {
		return pictureNumber;
	}

	public void setPictureNumber(int pictureNumber) {
		this.pictureNumber = pictureNumber;
	}

	public String getLoc() {
		return loc;
	}

	public void setLoc(String loc) {
		this.loc = loc;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public Contact(String name, int age, int picNumber) {
		this.firstName = name;
		this.age = age;
		this.pictureNumber = picNumber;

		//
		this.lastName = "Last Name";
		this.phoneNum = "555-555-5555";
		this.loc = "Address";
	}
	
	public Contact()
	{
		this.firstName = "First Name";
		this.age = 1;
		this.lastName = " ";
		this.phoneNum = "555-555-5555";
		this.pictureNumber = 1;
		this.loc = "Address";
	}
	
	public Contact(String name, String LastName, int Age, int picNumber, String PhoneNum, String address )
		{
		this.firstName = name;
		this.lastName = LastName;
		this.age = Age;
		this.phoneNum = PhoneNum;
		this.pictureNumber = picNumber;
		this.loc = address;
		}
	
	
		
	
	@Override
	public String toString(){
		return " First Name: "+this.firstName+" Last Name: "+this.lastName+
				" Phone Nummber: "+this.phoneNum+loc.toString()+this.pictureNumber+"\n";
	}


	@Override
	public int compareTo(Contact p)
		{
		int value = this.lastName.compareTo(p.getLastName());
		if (value == 0){
			return this.firstName.compareTo(p.getFirstName());
		}
		else{
			return value;
		}

	}
	
	
	

}
