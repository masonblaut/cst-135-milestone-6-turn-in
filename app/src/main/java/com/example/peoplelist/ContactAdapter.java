package com.example.peoplelist;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.milestone4.MyFriends;
import com.example.milestone4.model.Contact;

class ContactAdapter extends BaseAdapter {


    Activity mActivity;
    MyFriends myFriends;

    public ContactAdapter(Activity mActivity, MyFriends myFriends) {
        this.mActivity = mActivity;
        this.myFriends = myFriends;
    }

    //tells me how many people are in the list
    @Override
    public int getCount() {
        return myFriends.getMyFriendsList().size();
    }

    //will return a contact
    @Override
    public Contact getItem(int position) {
        return myFriends.getMyFriendsList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View oneContactLine;
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        oneContactLine = inflater.inflate(R.layout.person_one_line, parent, false);

        TextView tv_name = oneContactLine.findViewById(R.id.tv_name);
        TextView tv_age = oneContactLine.findViewById(R.id.tv_agevalue);
        ImageView iv_icon = oneContactLine.findViewById(R.id.iv_icon);
        TextView tv_lastName = oneContactLine.findViewById(R.id.tv_lastName);
        TextView tv_phonevalue = oneContactLine.findViewById(R.id.tv_phonevalue);

        Contact p = this.getItem(position);

        tv_name.setText(p.getFirstName());
        tv_age.setText(Integer.toString(p.getAge()));
        tv_lastName.setText(p.getLastName());
        tv_phonevalue.setText(p.getPhoneNum());

        int icon_resource_numbers [] = {
                R.drawable.bluedress,
                R.drawable.bowtie,
                R.drawable.brownhair,
                R.drawable.businesssuit,
                R.drawable.constructionworker,
                R.drawable.fancydress,
                R.drawable.fancysuit,
                R.drawable.farmer,
                R.drawable.girlglasses,
                R.drawable.greenstripes,
                R.drawable.greyhat,
                R.drawable.greyshirt,
                R.drawable.guyglasses,
                R.drawable.reddress,
                R.drawable.redhead,
                R.drawable.suitandtie
        };
        iv_icon.setImageResource(icon_resource_numbers[p.getPictureNumber()]);

        return oneContactLine;
    }
}
