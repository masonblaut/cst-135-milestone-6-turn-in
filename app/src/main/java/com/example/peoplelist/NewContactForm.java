package com.example.peoplelist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class NewContactForm extends AppCompatActivity {

    Button btn_ok, btn_cancel, btn_delete;
    EditText et_name, et_age, et_pictureNumber, et_lastName, et_phoneNum, et_address;
    ImageView iv_personicon;

    int positionToEdit = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_person_form);

        btn_ok = findViewById(R.id.btn_ok);
        btn_cancel = findViewById(R.id.btn_cancel);
        btn_delete = findViewById(R.id.btn_delete);
        et_name = findViewById(R.id.et_name);
        et_age = findViewById(R.id.et_age);
        et_pictureNumber = findViewById(R.id.et_picturenumber);
        et_address = findViewById(R.id.et_address);
        et_lastName = findViewById(R.id.et_lastName);
        et_phoneNum = findViewById(R.id.et_phoneNum);
        iv_personicon = (ImageView) findViewById(R.id.iv_personicon);

        //listen for incoming changes
        Bundle incomingMessages = getIntent().getExtras();

        if(incomingMessages != null){

            //capture incoming data
            String name = incomingMessages.getString("name");
            String lastName = incomingMessages.getString("lastName");
            int age = incomingMessages.getInt("age");
            int picNum = incomingMessages.getInt("pictureNumber");
            String phoneNum = incomingMessages.getString("phone");
            String address = incomingMessages.getString("address");
            positionToEdit = incomingMessages.getInt("edit");

            et_name.setText(name);
            et_lastName.setText(lastName);
            et_age.setText(Integer.toString(age));
            et_phoneNum.setText(phoneNum);
            et_address.setText(address);

            int icon_resource_numbers [] = {
                    R.drawable.bluedress,
                    R.drawable.bowtie,
                    R.drawable.brownhair,
                    R.drawable.businesssuit,
                    R.drawable.constructionworker,
                    R.drawable.fancydress,
                    R.drawable.fancysuit,
                    R.drawable.farmer,
                    R.drawable.girlglasses,
                    R.drawable.greenstripes,
                    R.drawable.greyhat,
                    R.drawable.greyshirt,
                    R.drawable.guyglasses,
                    R.drawable.reddress,
                    R.drawable.redhead,
                    R.drawable.suitandtie
            };
            iv_personicon.setImageResource(icon_resource_numbers[picNum]);
            et_pictureNumber.setText(Integer.toString(picNum));

        }

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newName = et_name.getText().toString();
                String newLastName = et_lastName.getText().toString();
                String newAge = et_age.getText().toString();
                String newPictureNumber = et_pictureNumber.getText().toString();
                String newPhoneNum = et_phoneNum.getText().toString();
                String newAddress = et_address.getText().toString();

                Intent i = new Intent();

                i.putExtra("edit", positionToEdit);
                i.putExtra("name", newName);
                i.putExtra("lastName", newLastName);
                i.putExtra("age", newAge);
                i.putExtra("pictureNumber", newPictureNumber);
                i.putExtra("phone", newPhoneNum);
                i.putExtra("address", newAddress);
                i.putExtra("delete", false);

                setResult(2, i);
                finish();
            }
        });

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (positionToEdit != -1){
                    String newName = et_name.getText().toString();
                    String newLastName = et_lastName.getText().toString();
                    String newAge = et_age.getText().toString();
                    String newPictureNumber = et_pictureNumber.getText().toString();
                    String newPhoneNum = et_phoneNum.getText().toString();
                    String newAddress = et_address.getText().toString();

                    Intent i = new Intent();

                    i.putExtra("edit", positionToEdit);
                    i.putExtra("name", newName);
                    i.putExtra("lastName", newLastName);
                    i.putExtra("age", newAge);
                    i.putExtra("pictureNumber", newPictureNumber);
                    i.putExtra("phone", newPhoneNum);
                    i.putExtra("address", newAddress);
                    i.putExtra("delete", true);

                    setResult(2, i);
                    finish();

                }
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
