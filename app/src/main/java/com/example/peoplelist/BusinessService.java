package com.example.peoplelist;
import android.content.Context;

import com.example.milestone4.MyFriends;
import com.example.milestone4.model.Contact;
import com.fasterxml.jackson.databind.ObjectMapper;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BusinessService {

    ObjectMapper om = new ObjectMapper();
    Context context;
    File homefile;

    public BusinessService (Context context) {
        this.context = context;
        File path = context.getExternalFilesDir(null);
        this.homefile = new File(path, "theContacts.json");
    };


    public void WriteAllData(MyFriends theList, String filename){

        File path = context.getExternalFilesDir(null);
        File file = new File(path, filename);

        try {
            om.writerWithDefaultPrettyPrinter().writeValue(file, theList);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public MyFriends ReadAllData(String filename){
        File path = context.getExternalFilesDir(null);
        File file = new File(path, filename);
        MyFriends returnList = new MyFriends();

        try{
            returnList = om.readValue(file, MyFriends.class);
        } catch (IOException e){
            e.printStackTrace();
        }

        return returnList;
    }

    public List<Contact> SearchFirstName(String name, MyFriends oldList){
        List<Contact> newList = new ArrayList<>();

        boolean hasMatch = false;

        for (int i = 0; i< oldList.getMyFriendsList().size(); i++){
            if (name.equals(oldList.getMyFriendsList().get(i).getFirstName())){
                newList.add(oldList.getMyFriendsList().get(i));
            }
        }

        return newList;
    }

}

