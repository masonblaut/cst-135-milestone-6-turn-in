package com.example.peoplelist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ContactProfile extends AppCompatActivity {

    Button btn_callNum, btn_loc, btn_exit;
    TextView tv_name, tv_age;
    ImageView iv_personicon;

    int position = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_profile);

        btn_callNum = (Button) findViewById(R.id.btn_callNum);
        btn_loc = (Button) findViewById(R.id.btn_loc);
        btn_exit = (Button) findViewById(R.id.btn_exit);

        tv_age = (TextView) findViewById(R.id.tv_age);
        tv_name = (TextView) findViewById(R.id.tv_name);

        iv_personicon = (ImageView) findViewById(R.id.iv_personicon);

        Bundle incomingMessages = getIntent().getExtras();

        if(incomingMessages != null){

            //capture incoming data
            String name = incomingMessages.getString("name");
            String lastName = incomingMessages.getString("lastName");
            String phone = incomingMessages.getString("phone");
            int age = incomingMessages.getInt("age");
            int picNum = incomingMessages.getInt("pictureNumber");
            String address = incomingMessages.getString("address");


            tv_name.setText(name+" "+lastName);
            tv_age.setText(Integer.toString(age));
            btn_callNum.setText(phone);
            btn_loc.setText(address);

            int icon_resource_numbers [] = {
                    R.drawable.bluedress,
                    R.drawable.bowtie,
                    R.drawable.brownhair,
                    R.drawable.businesssuit,
                    R.drawable.constructionworker,
                    R.drawable.fancydress,
                    R.drawable.fancysuit,
                    R.drawable.farmer,
                    R.drawable.girlglasses,
                    R.drawable.greenstripes,
                    R.drawable.greyhat,
                    R.drawable.greyshirt,
                    R.drawable.guyglasses,
                    R.drawable.reddress,
                    R.drawable.redhead,
                    R.drawable.suitandtie
            };
            iv_personicon.setImageResource(icon_resource_numbers[picNum]);
        }

        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
