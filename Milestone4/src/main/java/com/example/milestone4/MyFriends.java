package com.example.milestone4;

import com.example.milestone4.model.Contact;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class MyFriends {

    List<Contact> myFriendsList = new ArrayList<>();

    public MyFriends(){
    }

    public MyFriends(List<Contact> myFriendsList) {
        this.myFriendsList = myFriendsList;
    }

    public <T extends Contact> void addOne(T newContact){
        this.myFriendsList.add(newContact);
    }

    public <T extends Contact> boolean deleteOne(T oldContact){
        if (this.myFriendsList.contains(oldContact)){
            this.myFriendsList.remove(oldContact);
            return true;
        }
        else{
            return false;
        }
    }


    public MyFriends(String[] startingNames){
        this.myFriendsList = new ArrayList<>();
        Random rng = new Random();
        for (int i =0; i<startingNames.length; i++){
            Contact p = new Contact(startingNames[i], rng.nextInt(50)+15, rng.nextInt(16));
            myFriendsList.add(p);
        }
    }

    public List<Contact> getMyFriendsList() {
        return myFriendsList;
    }

    public void setMyFriendsList(List<Contact> myFriendsList) {
        this.myFriendsList = myFriendsList;
    }

    @Override
    public String toString() {
        return "MyFriends{" +
                "myFriendsList=" + myFriendsList +
                '}';
    }
}
