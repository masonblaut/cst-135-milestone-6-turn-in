package com.example.peoplelist;

import android.app.Application;

import com.example.milestone4.MyFriends;

public class MyApplication extends Application {

    String[] defaults = {"Abby", "Bob", "Echo"};
    private MyFriends myFriends = new MyFriends(defaults);

    public MyFriends getMyFriends() {
        return myFriends;
    }

    public void setMyFriends(MyFriends myFriends) {
        this.myFriends = myFriends;
    }
}
